import bpy
from bpy_extras.io_utils import ImportHelper 
from bpy.types import Operator
from bpy.props import StringProperty
import os
import mathutils
import nodeitems_utils


# Thomas : ouvrir la fenêtre d'importation de l'image
class OT_ImportStickerImage(Operator, ImportHelper):
    bl_idname = "test.open_filebrowser" 
    bl_label = "Import Sticker Image" 
    
    filter_glob: StringProperty(
        default='*.jpg;*.jpeg;*.png;*.tif;*.tiff;*.bmp',
        options={'HIDDEN'} 
    )

    def execute(self, context): 
        """Do something with the selected file(s).""" 
        
        filename, extension = os.path.splitext(self.filepath) 
        file_name = os.path.basename(self.filepath)
        
        print('Selected file:', self.filepath)
        print('File name:', file_name) 
        print('File extension:', extension)
        
        bpy.ops.image.open(filepath=self.filepath)
        sticker_image = bpy.data.images.get(file_name)
        
        # calculer l'aspect_ratio 
        aspect_ratio = sticker_image.size[0] / sticker_image.size[1]
        
        mat_nodes = bpy.context.space_data.edit_tree.nodes
        
        #créer le node mix shader et le relier au nodes précedents

        node_output = mat_nodes.get("Material Output")
    
        node_ShaderToMatOutput = node_output.inputs[0].links[0].from_node
    
        node_mixShader = mat_nodes.new("ShaderNodeMixShader")
        node_mixShader.location = node_ShaderToMatOutput.location + mathutils.Vector((400,200))
        node_output.location = node_mixShader.location + mathutils.Vector((200,0))
        
        #créer le node group avec input et outputs
        
        sticker_group_tree = bpy.data.node_groups.new('StickerGroup', 'ShaderNodeTree')
    
        sticker_group_node = mat_nodes.new("ShaderNodeGroup")
        sticker_group_node.location = node_mixShader.location + mathutils.Vector((-400,300))
    
        sticker_group_node.node_tree = bpy.data.node_groups[sticker_group_tree.name]
        
        #creer les nodes dans le node group
    
        group_inputs = sticker_group_tree.nodes.new('NodeGroupInput')
        sticker_group_tree.interface.new_socket(name="Location",in_out='INPUT',socket_type='NodeSocketVector')
        sticker_group_tree.interface.new_socket(name="Rotation",in_out='INPUT',socket_type='NodeSocketVector')
        sticker_group_tree.interface.new_socket(name="Scale",in_out='INPUT',socket_type='NodeSocketVector')
        sticker_group_tree.interface.new_socket(name="Roughness",in_out='INPUT',socket_type='NodeSocketFloat')
        sticker_group_tree.interface.new_socket(name="Transparency",in_out='INPUT',socket_type='NodeSocketFloat')
    
        node_texCoordinate = sticker_group_tree.nodes.new(type="ShaderNodeTexCoord")
        node_texCoordinate.location = group_inputs.location + mathutils.Vector((300,0))
        
        node_mapping = sticker_group_tree.nodes.new(type="ShaderNodeMapping")
        node_mapping.location = node_texCoordinate.location + mathutils.Vector((300,0))
        # application de l'aspect ratio pour contrebalancer la déformation de l'UV Unwrap
        node_mapping.inputs[3].default_value = (1,1*aspect_ratio,1)
        
        node_img_texture = sticker_group_tree.nodes.new(type="ShaderNodeTexImage")
        node_img_texture.location = node_mapping.location + mathutils.Vector((300,0))
        node_img_texture.extension = 'CLIP'
        node_img_texture.image=sticker_image
        
        node_mix_color = sticker_group_tree.nodes.new("ShaderNodeMixRGB")
        node_mix_color.inputs[0].default_value = (0)
        node_mix_color.inputs[2].default_value = (0,0,0,1)
        node_mix_color.location = node_img_texture.location + mathutils.Vector((300,-300))
        
        node_sticker_BSDF = sticker_group_tree.nodes.new("ShaderNodeBsdfPrincipled")
        node_sticker_BSDF.location = node_img_texture.location + mathutils.Vector((300,0))
        
        group_outputs = sticker_group_tree.nodes.new('NodeGroupOutput')
        sticker_group_tree.interface.new_socket(name='Shader',in_out='OUTPUT',socket_type='NodeSocketShader')
        sticker_group_tree.interface.new_socket(name='Color',in_out='OUTPUT',socket_type='NodeSocketColor')
        group_outputs.location = node_sticker_BSDF.location + mathutils.Vector((400,0))
        #relier les nodes du node group
        
        group_links = sticker_group_tree.links
        
        group_links.new(node_texCoordinate.outputs[2], node_mapping.inputs[0])
        group_links.new(node_mapping.outputs[0], node_img_texture.inputs[0])
        group_links.new(node_img_texture.outputs[0], node_sticker_BSDF.inputs[0])
        group_links.new(node_img_texture.outputs[1], node_mix_color.inputs[1])
        
        group_links.new(group_inputs.outputs[0], node_mapping.inputs[1])
        group_links.new(group_inputs.outputs[1], node_mapping.inputs[2])
        group_links.new(group_inputs.outputs[2], node_mapping.inputs[3])
        group_links.new(group_inputs.outputs[3], node_sticker_BSDF.inputs[2])
        group_links.new(group_inputs.outputs[4], node_mix_color.inputs[0])
        
        group_links.new(node_sticker_BSDF.outputs[0], group_outputs.inputs['Shader'])
        group_links.new(node_mix_color.outputs[0], group_outputs.inputs['Color'])

        #relier les nodes du material

        links = bpy.context.space_data.edit_tree.links
        
        links.new(sticker_group_node.outputs[0], node_mixShader.inputs[2])
        links.new(sticker_group_node.outputs[1], node_mixShader.inputs[0])
        links.new(node_ShaderToMatOutput.outputs[0], node_mixShader.inputs[1])
        links.new(node_mixShader.outputs[0], node_output.inputs[0])
        
        sticker_group_node.inputs[2].default_value = (1.0,1.0,1.0)
        
        links.new(node_img_texture.outputs[0], node_sticker_BSDF.inputs[0])
        links.new(node_img_texture.outputs[1], node_mix_color.inputs[1])
        
        links.new(node_mix_color.outputs[0],node_mixShader.inputs[0])
        
        links.new(node_mapping.outputs[0], node_img_texture.inputs[0])
        links.new(node_texCoordinate.outputs[2], node_mapping.inputs[0])
        
        
        node_ShaderToMatOutput = node_output.inputs[0].links[0].from_node
        
        
        return {'FINISHED'}  
    
    
class MCN_PT_TT_QG(bpy.types.Panel):
    """"""
    bl_label = "TT_QG"
    bl_idname = "MCN_PT_TT_QG"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    bl_category = "Tool"
    #bl_parent_id = "MCN_PT_Main_Panel"
    bl_options = {'DEFAULT_CLOSED'}
    
    def draw(self, context):
        layout = self.layout

        # TODO Replace this label by your stuff here
        layout.label(text="See TT_QG's Sticker Maker tool in Shader Editor")

class MCN_PT_TT_QG_TRUE(bpy.types.Panel):
    """"""
    bl_label = "TT_QG"
    bl_idname = "Import Sticker"
    bl_space_type = 'NODE_EDITOR'
    bl_region_type = 'UI'
    bl_category = "Import Sticker"
    #bl_parent_id = "MCN_PT_Main_Panel"
    bl_options = {'DEFAULT_CLOSED'}

    def draw(self, context):
        layout = self.layout

        # TODO Replace this label by your stuff here
        layout.label(text="TT_QG’s tools here")
        row_sticker = layout.row()
        row_sticker.operator("test.open_filebrowser",text="Import Sticker", icon="IMAGE_PLANE")


def register():
    bpy.utils.register_class(MCN_PT_TT_QG)
    bpy.utils.register_class(OT_ImportStickerImage)
    bpy.utils.register_class(MCN_PT_TT_QG_TRUE)


def unregister():
    bpy.utils.unregister_class(MCN_PT_TT_QG)
    bpy.utils.unregister_class(OT_ImportStickerImage)
    bpy.utils.unregister_class(MCN_PT_TT_QG_TRUE)

if __name__ == "__main__":
    register()